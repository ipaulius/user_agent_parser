module bitbucket.org/soundest/user_agent_parser

go 1.16

require (
	bitbucket.org/soundest/go-log v2.3.6+incompatible
	github.com/glenn-brown/golang-pkg-pcre v0.0.0-20120522223659-48bb82a8b8ce // indirect
	github.com/mattn/go-sqlite3 v1.14.8 // indirect
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/udger/udger v0.0.0-20170323112903-41a723f8095d
)
