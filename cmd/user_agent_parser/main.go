package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"

	sLog "bitbucket.org/soundest/go-log"
	"bitbucket.org/soundest/user_agent_parser/internal/entities"
	"bitbucket.org/soundest/user_agent_parser/internal/parsers"
	"github.com/udger/udger"
)

func main() {
	srcFile := flag.String("src", "input.json", "json user agent source file")
	flag.Parse()

	stats := entities.NewStats()

	resultCh := parse(*srcFile)

	for rez := range resultCh {
		stats.Aggregate(&rez)

		//b, _ := json.MarshalIndent(rez, "", "  ")
		//fmt.Printf("%s\n", string(b))
	}

	fmt.Println()
	fmt.Println("//")
	fmt.Println("// User agent match stats by browser family:")
	fmt.Println("//")

	b, _ := json.MarshalIndent(stats, "", "  ")
	fmt.Printf("%s\n", string(b))
}

func parse(filePath string) chan entities.Browser {
	ch := make(chan entities.Browser, 100)

	parserList := map[string]parsers.Parser{
		"udger": createUdgerParser(),
		"map":   parsers.NewMapParser(),
		"light": parsers.NewLightMatchParser(),
	}

	go func() {
		count := 0
		for line := range readLines(filePath) {
			for k, parser := range parserList {
				m, _ := parser.GetUserAgentMetadata(line)

				rez := entities.Browser{
					UserAgent:     line,
					Parser:        k,
					BrowserFamily: m["browser_family"],
					BrowserType:   m["browser_type"],
				}

				ch <- rez
			}

			count++
			if count%100 == 0 {
				fmt.Printf("Processed: %d\n", count)
			}
		}

		fmt.Printf("Total processed: %d\n", count)

		close(ch)
	}()

	return ch
}

func createUdgerParser() parsers.Parser {
	db, err := udger.New("./assets/udgerdb_v3.dat")
	if err != nil {
		sLog.Data(sLog.M{}).Error(500, err)
		os.Exit(-1)
	}

	return parsers.NewUdgerParser(db)
}

func readLines(filePath string) chan string {
	ch := make(chan string, 100)

	go func() {
		file, err := os.Open(filePath)
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()

		scanner := bufio.NewScanner(file)

		// optionally, resize scanner's capacity for lines over 64K, see next example
		for scanner.Scan() {
			ch <- scanner.Text()
		}

		if err := scanner.Err(); err != nil {
			log.Fatal(err)
		}

		close(ch)
	}()

	//go func() {
	//	ch <- "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36"
	//	ch <- "Mozilla/5.0 (Linux; U; Android 4.4.2; en-us; SCH-I535 Build/KOT49H) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30"
	//	ch <- "Mozilla/5.0 (iPhone; CPU iPhone OS 14_6 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko)"
	//	close(ch)
	//}()

	return ch
}
