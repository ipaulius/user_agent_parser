package entities

type Browser struct {
	BrowserFamily string `json:"browser_family"`
	BrowserType   string `json:"browser_type"`

	Parser    string `json:"parser"`
	UserAgent string `json:"user_agent"`
}
