package entities

import "strings"

type Stats struct {
	BrowserStats map[string]*BrowserStats `json:"browserStats"`
}

type BrowserStats struct {
	BrowserFamily string         `json:"browser_family"`
	BrowserType   string         `json:"browser_type"`
	ParserMatch   map[string]int `json:"parser_match"`
}

func NewStats() *Stats {
	return &Stats{
		BrowserStats: map[string]*BrowserStats{},
	}
}

func (r *Stats) Aggregate(browser *Browser) {
	key := getBrowserKey(browser)
	s, ok := r.BrowserStats[key]
	if !ok {
		s = &BrowserStats{
			BrowserFamily: browser.BrowserFamily,
			BrowserType:   browser.BrowserType,
			ParserMatch:   map[string]int{},
		}

		r.BrowserStats[key] = s
	}

	_, ok = s.ParserMatch[browser.Parser]
	if !ok {
		s.ParserMatch[browser.Parser] = 0
	}

	s.ParserMatch[browser.Parser]++
}

func getBrowserKey(browser *Browser) string {
	//return browser.BrowserFamily
	return strings.Join([]string{browser.BrowserFamily, browser.BrowserType}, " / ")
}
