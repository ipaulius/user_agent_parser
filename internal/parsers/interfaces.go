package parsers

type Parser interface {
	GetUserAgentMetadata(userAgent string) (map[string]string, error)
}
