package parsers

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCached(t *testing.T) {
	userAgent := "User-Agent Test"

	t.Run("returns cached response", func(t *testing.T) {
		cachedResponse := map[string]string{
			"cached": "yes",
		}

		repositoryResponse := map[string]string{
			"cached": "no",
		}

		// Setup cache with already cached response
		cache := cache.NewMapCache()
		cache.Set(userAgent, cachedResponse)

		// Setup repository with response different than the cached one
		stubRepository := &userAgentStubRepository{}
		stubRepository.mockedResponse = repositoryResponse
		cachedRepository := NewUserAgentCachedRepository(stubRepository, cache)

		response, err := cachedRepository.GetUserAgentMetadata(context.TODO(), userAgent)

		// Assert response is from cache
		assert.NoError(t, err)
		assert.True(t, assert.ObjectsAreEqual(cachedResponse, response))
	})

	t.Run("context no-cache flag ignores cached response", func(t *testing.T) {
		ctx := cache.SetNoCache(context.TODO(), true)

		cachedResponse := map[string]string{
			"cached": "yes",
		}

		repositoryResponse := map[string]string{
			"cached": "no",
		}

		// Setup cache with already cached response
		cache := cache.NewMapCache()
		cache.Set(userAgent, cachedResponse)

		// Setup repository with response different than the cached one
		stubRepository := &userAgentStubRepository{}
		stubRepository.mockedResponse = repositoryResponse
		cachedRepository := NewUserAgentCachedRepository(stubRepository, cache)

		response, err := cachedRepository.GetUserAgentMetadata(ctx, userAgent)

		// Assert response is not from cache
		assert.NoError(t, err)
		assert.True(t, assert.ObjectsAreEqual(repositoryResponse, response))
	})

	t.Run("context no-cache flag does not cache response", func(t *testing.T) {
		ctx := cache.SetNoCache(context.TODO(), true)
		cache := cache.NewMapCache()

		repositoryResponse := map[string]string{
			"cached": "no",
		}

		stubRepository := &userAgentStubRepository{}
		stubRepository.mockedResponse = repositoryResponse
		cachedRepository := NewUserAgentCachedRepository(stubRepository, cache)

		response, _ := cachedRepository.GetUserAgentMetadata(ctx, userAgent)
		assert.True(t, assert.ObjectsAreEqual(repositoryResponse, response))

		// Assert response was not cached
		cachedObject, found := cache.Get(userAgent)
		assert.False(t, found)
		assert.Nil(t, cachedObject)
	})

	t.Run("does not cache response on error", func(t *testing.T) {
		cache := cache.NewMapCache()

		repositoryResponse := map[string]string{
			"cached": "no",
		}

		// Setup service to return error
		stubRepository := &userAgentStubRepository{}
		stubRepository.mockedResponse = repositoryResponse
		stubRepository.mockedError = fmt.Errorf("mocked error")
		cachedRepository := NewUserAgentCachedRepository(stubRepository, cache)

		_, err := cachedRepository.GetUserAgentMetadata(context.TODO(), userAgent)
		assert.Error(t, err)
		assert.Equal(t, "mocked error", err.Error())

		// Assert response was not cached when error was returned
		cachedObject, found := cache.Get(userAgent)
		assert.False(t, found)
		assert.Nil(t, cachedObject)
	})

	t.Run("successful response is cached", func(t *testing.T) {
		repositoryResponse := map[string]string{
			"cached":  "no",
			"success": "yes",
		}

		cache := cache.NewMapCache()

		stubRepository := &userAgentStubRepository{}
		stubRepository.mockedResponse = repositoryResponse
		cachedRepository := NewUserAgentCachedRepository(stubRepository, cache)

		response, _ := cachedRepository.GetUserAgentMetadata(context.TODO(), userAgent)
		assert.True(t, assert.ObjectsAreEqual(repositoryResponse, response))

		// Assert response was cached:
		cachedObject, found := cache.Get(userAgent)
		if assert.True(t, found) {
			cachedResponse, ok := cachedObject.(map[string]string)

			assert.True(t, ok)
			assert.NotNil(t, cachedResponse)
			assert.True(t, assert.ObjectsAreEqual(cachedResponse, response))
			assert.True(t, assert.ObjectsAreEqual(repositoryResponse, response))
		}
	})
}

type userAgentStubRepository struct {
	mockedResponse map[string]string
	mockedError    error
}

func (r *userAgentStubRepository) GetUserAgentMetadata(ctx context.Context, userAgent string) (map[string]string, error) {
	return r.mockedResponse, r.mockedError
}
