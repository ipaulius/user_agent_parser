package parsers

import (
	sLog "bitbucket.org/soundest/go-log"
	"github.com/udger/udger"
)

type udgerParser struct {
	db *udger.Udger
}

func NewUdgerParser(db *udger.Udger) Parser {
	return &udgerParser{
		db: db,
	}
}

func (r *udgerParser) GetUserAgentMetadata(userAgent string) (map[string]string, error) {
	info, err := r.db.Lookup(userAgent)
	if err != nil {
		sLog.Data(sLog.M{"user-agent": userAgent}).Error(400, userAgent)
		return nil, err
	}

	result := map[string]string{
		"browser_family": info.Browser.Family,
		"browser_type":   info.Browser.Type,
	}

	return result, nil
}
