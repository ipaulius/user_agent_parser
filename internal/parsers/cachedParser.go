package parsers

import (
	"context"
	"time"

	sApm "bitbucket.org/soundest/go-apm"
)

const (
	DefaultCacheTTLDuration = time.Hour * 24

	cacheHitCounterName   = "userAgentRepository"
	cacheHitCounterTagKey = "cache"
)

type userAgentCachedRepository struct {
	repository UserAgentRepository
	cache      cache.Cache
}

func NewUserAgentCachedRepository(repository UserAgentRepository, cache cache.Cache) UserAgentRepository {
	return &userAgentCachedRepository{
		repository: repository,
		cache:      cache,
	}
}

func (r *userAgentCachedRepository) GetUserAgentMetadata(ctx context.Context, userAgent string) (map[string]string, error) {
	skipCache := cache.GetNoCache(ctx)
	if !skipCache {
		if result, cacheHit := r.getFromCache(userAgent); cacheHit {
			sApm.TrackCount(cacheHitCounterName, 1, sApm.Tag{Key: cacheHitCounterTagKey, Value: "hit"})
			return result, nil
		}
	}

	sApm.TrackCount(cacheHitCounterName, 1, sApm.Tag{Key: cacheHitCounterTagKey, Value: "miss"})

	result, err := r.repository.GetUserAgentMetadata(ctx, userAgent)
	if !skipCache && err == nil {
		r.cache.Set(userAgent, result)
	}

	return result, err
}

func (r *userAgentCachedRepository) getFromCache(userAgent string) (map[string]string, bool) {
	if cached, exists := r.cache.Get(userAgent); exists {
		if result, ok := cached.(map[string]string); ok {
			return result, true
		}
	}

	return nil, false
}
