package parsers

import "strings"

type lightMatchParser struct {
	patterns []Pattern
}

type Pattern struct {
	Substrings []string

	BrowserFamily string
	BrowserType   string

	Properties map[string]string
}

func (r *Pattern) match(userAgent string) (match bool) {
	match = false
	for _, p := range r.Substrings {
		index := strings.Index(userAgent, p)
		if index < 0 {
			return
		}
	}
	match = true
	return
}

func NewLightMatchParser() Parser {
	return &lightMatchParser{
		patterns: createPatterns(),
	}
}

func (r *lightMatchParser) GetUserAgentMetadata(userAgent string) (map[string]string, error) {
	for _, p := range r.patterns {
		if p.match(userAgent) {
			return p.Properties, nil
		}
	}

	// Empty or default response:
	return nil, nil
}

func createPatterns() []Pattern {
	return []Pattern{
		{
			Substrings: []string{"Mozilla", "Mac", "AppleWebKit", "(KHTML, like Gecko)"},
			Properties: map[string]string{
				"browser_family": "Apple Mail",
				"browser_type":   "E-mail client",
			},
		},
		{
			Substrings: []string{"Android", "Mail"},
			Properties: map[string]string{
				"browser_family": "Gmail App",
				"browser_type":   "",
			},
		},
	}
}
