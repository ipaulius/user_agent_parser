//nolint:dupl
package cache

type MapCache struct {
	cache map[string]interface{}
}

func NewMapCache() Cache {
	return &MapCache{
		cache: map[string]interface{}{},
	}
}

func (r *MapCache) Set(key string, value interface{}) {
	r.cache[key] = value
}

func (r *MapCache) Get(key string) (value interface{}, exists bool) {
	value, exists = r.cache[key]
	return
}
