package cache

import (
	"context"
)

type (
	noCacheDirectiveKeyType string
)

var (
	noCacheDirectiveKey noCacheDirectiveKeyType = "no-cache"
)

type Cache interface {
	Set(key string, value interface{})
	Get(key string) (value interface{}, exists bool)
}

func GetNoCache(ctx context.Context) (noCache bool) {
	if noCacheValue, ok := ctx.Value(noCacheDirectiveKey).(bool); ok {
		noCache = noCacheValue
	}

	return
}

func SetNoCache(ctx context.Context, noCache bool) context.Context {
	return context.WithValue(ctx, noCacheDirectiveKey, noCache)
}
