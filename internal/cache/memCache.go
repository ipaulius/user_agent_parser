//nolint:dupl
package cache

import (
	"log"
	"time"

	"github.com/dgraph-io/ristretto"
)

type MemCache struct {
	cache       *ristretto.Cache
	ttlDuration time.Duration
}

func NewMemCache(ttlDuration time.Duration) Cache {
	cache, err := ristretto.NewCache(&ristretto.Config{
		NumCounters: 1e6,     // number of keys to track frequency of (1M).
		MaxCost:     3 << 25, // maximum cost of cache (100MB).
		BufferItems: 64,      // number of keys per Get buffer. Suggested 64
	})

	if err != nil {
		log.Printf("failed to create cache: %s\n", err.Error())
		return nil
	}

	return &MemCache{
		cache:       cache,
		ttlDuration: ttlDuration,
	}
}

func (r *MemCache) Set(key string, value interface{}) {
	r.cache.SetWithTTL(key, value, 1, r.ttlDuration)
}

func (r *MemCache) Get(key string) (value interface{}, exists bool) {
	return r.cache.Get(key)
}
