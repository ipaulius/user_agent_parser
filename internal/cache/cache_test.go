package cache

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestCache(t *testing.T) {

	testCases := []struct {
		Name     string
		NewCache func() Cache
		Wait     time.Duration
	}{
		{
			Name:     "MemCache",
			NewCache: func() Cache { return NewMemCache(10 * time.Second) },
			Wait:     time.Millisecond * 10,
		},
		{
			Name:     "MapCache",
			NewCache: NewMapCache,
			Wait:     0,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.Name, func(t *testing.T) {

			t.Run("empty cache returns has no value for key", func(t *testing.T) {
				cache := tc.NewCache()

				value, exists := cache.Get("my key")
				assert.False(t, exists)
				assert.Nil(t, value)
			})

			t.Run("key value is cached ", func(t *testing.T) {
				cache := tc.NewCache()

				var (
					key   = "my key"
					value = "my cached string"
				)

				cache.Set(key, value)

				time.Sleep(tc.Wait) // wait for cached value to pass through buffers

				cachedValue, exists := cache.Get(key)
				assert.True(t, exists)
				assert.Equal(t, value, cachedValue)
			})

			t.Run("multiple key values does not collide", func(t *testing.T) {
				cache := tc.NewCache()

				var (
					key1   = "my key 1"
					value1 = "my cached string 1"

					key2   = "my key 2"
					value2 = "my cached string 2"
				)

				cache.Set(key1, value1)
				cache.Set(key2, value2)

				time.Sleep(tc.Wait) // wait for cached value to pass through buffers

				cachedValue, exists := cache.Get(key1)
				assert.True(t, exists)
				assert.Equal(t, value1, cachedValue)

				cachedValue, exists = cache.Get(key2)
				assert.True(t, exists)
				assert.Equal(t, value2, cachedValue)
			})
		})
	}
}

func TestNoCache(t *testing.T) {
	t.Run("no cache passed via context", func(t *testing.T) {
		ctx := context.TODO()

		defaultNoCache := GetNoCache(ctx)
		assert.False(t, defaultNoCache)

		ctx = SetNoCache(ctx, true)
		assert.True(t, GetNoCache(ctx))

		ctx = SetNoCache(ctx, false)
		assert.False(t, GetNoCache(ctx))
	})
}
